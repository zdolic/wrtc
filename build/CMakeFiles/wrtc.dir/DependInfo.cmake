# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/test/node_modules/wrtc/src/binding.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/binding.cc.o"
  "/home/pi/test/node_modules/wrtc/src/converters/webrtc.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/converters/webrtc.cc.o"
  "/home/pi/test/node_modules/wrtc/src/createsessiondescriptionobserver.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/createsessiondescriptionobserver.cc.o"
  "/home/pi/test/node_modules/wrtc/src/datachannel.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/datachannel.cc.o"
  "/home/pi/test/node_modules/wrtc/src/events.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/events.cc.o"
  "/home/pi/test/node_modules/wrtc/src/mediastream.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/mediastream.cc.o"
  "/home/pi/test/node_modules/wrtc/src/mediastreamtrack.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/mediastreamtrack.cc.o"
  "/home/pi/test/node_modules/wrtc/src/peerconnection.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/peerconnection.cc.o"
  "/home/pi/test/node_modules/wrtc/src/peerconnectionfactory.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/peerconnectionfactory.cc.o"
  "/home/pi/test/node_modules/wrtc/src/rtcrtpreceiver.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/rtcrtpreceiver.cc.o"
  "/home/pi/test/node_modules/wrtc/src/rtcrtpsender.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/rtcrtpsender.cc.o"
  "/home/pi/test/node_modules/wrtc/src/rtcstatsreport.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/rtcstatsreport.cc.o"
  "/home/pi/test/node_modules/wrtc/src/rtcstatsresponse.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/rtcstatsresponse.cc.o"
  "/home/pi/test/node_modules/wrtc/src/setsessiondescriptionobserver.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/setsessiondescriptionobserver.cc.o"
  "/home/pi/test/node_modules/wrtc/src/stats-observer.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/stats-observer.cc.o"
  "/home/pi/test/node_modules/wrtc/src/webrtc/fake_audio_device.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/webrtc/fake_audio_device.cc.o"
  "/home/pi/test/node_modules/wrtc/src/webrtc/physicalsocketserver.cc" "/home/pi/test/node_modules/wrtc/build/CMakeFiles/wrtc.dir/src/webrtc/physicalsocketserver.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "MODULE_NAME=wrtc"
  "WEBRTC_LINUX"
  "WEBRTC_POSIX=1"
  "WRTC_BUILD"
  "_FILE_OFFSET_BITS=64"
  "_GLIBCXX_USE_CXX11_ABI=0"
  "_LARGEFILE_SOURCE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "node/v8.12.0/include"
  "../node_modules/nan"
  "../"
  "../third_party/webrtc/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
