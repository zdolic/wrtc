
[![NPM](https://img.shields.io/npm/v/wrtc.svg)](https://www.npmjs.com/package/wrtc) [![macOS/Linux Build Status](https://secure.travis-ci.org/js-platform/node-webrtc.svg?branch=develop)](http://travis-ci.org/js-platform/node-webrtc) [![Windows Build status](https://ci.appveyor.com/api/projects/status/iulc84we28o1i7b9?svg=true)](https://ci.appveyor.com/project/markandrus/node-webrtc-7bnua)

node-webrtc provides Node.js bindings to [WebRTC M70](https://github.com/mayeut/libwebrtc/releases/tag/v1.1.1). You can write Node.js applications that use RTCDataChannels with it. **Some MediaStream APIs are supported now!.**

|         | x86 | x64 | arm | arm64 |
|:------- |:--- |:--- |:--- |:----- |
| Linux   |     |     |     |  ✔︎    |
| macOS   |     |     |     |       |
| Windows |     |     |     |       |

# Getting Started

## Prerequisites

This library contains builded "node-wrtc" binaries for platform raspberry pi 3 (strech). To use, just unpack into node_modules/wrtc folder and then require it as the package is installed via "npm install wrtc@0.2.1"


